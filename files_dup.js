var files_dup =
[
    [ "CLPCStepResponsePage.py", "CLPCStepResponsePage_8py.html", null ],
    [ "CLProportionalController.py", "CLProportionalController_8py.html", [
      [ "CLProportionalController", "classCLProportionalController_1_1CLProportionalController.html", "classCLProportionalController_1_1CLProportionalController" ]
    ] ],
    [ "CLProportionalControllerTest.py", "CLProportionalControllerTest_8py.html", "CLProportionalControllerTest_8py" ],
    [ "ElevatorTask.py", "ElevatorTask_8py.html", [
      [ "ElevatorStates", "classElevatorTask_1_1ElevatorStates.html", null ],
      [ "ElevatorTask", "classElevatorTask_1_1ElevatorTask.html", "classElevatorTask_1_1ElevatorTask" ]
    ] ],
    [ "Encoder.py", "Encoder_8py.html", "Encoder_8py" ],
    [ "FinalProductDocumentation.py", "FinalProductDocumentation_8py.html", null ],
    [ "IMUDriver.py", "IMUDriver_8py.html", [
      [ "IMUDriver", "classIMUDriver_1_1IMUDriver.html", "classIMUDriver_1_1IMUDriver" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "Proposal.py", "Proposal_8py.html", null ],
    [ "ProximityDriver.py", "ProximityDriver_8py.html", [
      [ "ProximityDriver", "classProximityDriver_1_1ProximityDriver.html", "classProximityDriver_1_1ProximityDriver" ]
    ] ],
    [ "TermProject.py", "TermProject_8py.html", null ],
    [ "TurntableTask.py", "TurntableTask_8py.html", [
      [ "TurntableStates", "classTurntableTask_1_1TurntableStates.html", null ],
      [ "TurntableTask", "classTurntableTask_1_1TurntableTask.html", "classTurntableTask_1_1TurntableTask" ]
    ] ]
];