var CLProportionalControllerTest_8py =
[
    [ "controller", "CLProportionalControllerTest_8py.html#a575d92d2506acc64c896476503f7edbd", null ],
    [ "count", "CLProportionalControllerTest_8py.html#a738a73de2bae6ac5563a5990d5d7a8bd", null ],
    [ "desired", "CLProportionalControllerTest_8py.html#a8d034a310e3f2683e0f4ae744f33a7d6", null ],
    [ "duty", "CLProportionalControllerTest_8py.html#ae964baef64743b57758e0600d4b42f83", null ],
    [ "encoder", "CLProportionalControllerTest_8py.html#aee618342924d27ffaa6e03e26c1f6867", null ],
    [ "encoderPin1", "CLProportionalControllerTest_8py.html#a7dcfc53d9185a37d4c777b0ff11b5efa", null ],
    [ "encoderPin2", "CLProportionalControllerTest_8py.html#ab94af2eddc97317252bc4de9cb2af861", null ],
    [ "kp", "CLProportionalControllerTest_8py.html#a91e0d1b92f10203dbbce7c4d42feffb9", null ],
    [ "loops", "CLProportionalControllerTest_8py.html#a68f13db7575317a11b4891a262adb770", null ],
    [ "motor", "CLProportionalControllerTest_8py.html#a11b52cc28b8af7cb735309a0f7896e7e", null ],
    [ "newDuty", "CLProportionalControllerTest_8py.html#af7babe9db82e51f7c81e80c16622cd43", null ],
    [ "pin_EN", "CLProportionalControllerTest_8py.html#ab938ad3d6fa2b158a30da1181922d802", null ],
    [ "pin_IN1", "CLProportionalControllerTest_8py.html#af9b1b0eb4f749a21b42cacaa1dd21cfc", null ],
    [ "pin_IN2", "CLProportionalControllerTest_8py.html#a9ee80c144514b20099a5253f016aa53b", null ],
    [ "position", "CLProportionalControllerTest_8py.html#ad084b08fa08454c6310e212da03111e8", null ],
    [ "setpoint", "CLProportionalControllerTest_8py.html#a012e924daceff3f55366e51fe5d703ad", null ],
    [ "startTime", "CLProportionalControllerTest_8py.html#a01dc02839eb30d01d9b8f7f974bc8e91", null ],
    [ "stepFinalValue", "CLProportionalControllerTest_8py.html#a475de858bc4716fa98c924503fe7b72d", null ],
    [ "stepInitialValue", "CLProportionalControllerTest_8py.html#a8976a949e8bda8661326cc4b1d352279", null ],
    [ "stepTime", "CLProportionalControllerTest_8py.html#a14be90bfa6105e3b9be8fb4a5c041f6d", null ],
    [ "time", "CLProportionalControllerTest_8py.html#a8ac3b1d2573ca4dc8dc67624cdaf81f4", null ],
    [ "timer", "CLProportionalControllerTest_8py.html#a91cdf937dae1520c88081192180dfc95", null ],
    [ "updatePeriod", "CLProportionalControllerTest_8py.html#a7df95bd84cc03ed85e7e5dbb0479976d", null ]
];