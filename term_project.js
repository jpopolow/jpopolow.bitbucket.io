var term_project =
[
    [ "Requirements", "term_project.html#term_project_requirements", null ],
    [ "Project Proposal", "term_project_proposal.html", [
      [ "Problem Statement", "term_project_proposal.html#problem_statement", null ],
      [ "Overview", "term_project_proposal.html#term_project_overview", null ],
      [ "Requirement Complience", "term_project_proposal.html#requirement_complience", null ],
      [ "Materials", "term_project_proposal.html#term_project_materials", [
        [ "Electronics", "term_project_proposal.html#Electronics", null ],
        [ "Construction", "term_project_proposal.html#Construction", null ]
      ] ],
      [ "Manufacturing and Assembly Plan", "term_project_proposal.html#assembly_plan", null ],
      [ "Safety", "term_project_proposal.html#term_project_safety", null ],
      [ "Timeline", "term_project_proposal.html#Timeline", null ]
    ] ],
    [ "Final Product", "term_project_final_product.html", [
      [ "Project Scope and Completion", "term_project_final_product.html#scope_and_completion", null ],
      [ "Video Demonstration", "term_project_final_product.html#term_project_demonstration", null ],
      [ "Firmware Documentation", "term_project_final_product.html#term_project_documentation", null ]
    ] ]
];