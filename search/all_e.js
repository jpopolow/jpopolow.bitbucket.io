var searchData=
[
  ['set_5fduty_64',['set_duty',['../classMotorDriver_1_1MotorDriver.html#a4bb86eafa05d8e874896aef624ad14cd',1,'MotorDriver::MotorDriver']]],
  ['set_5fposition_65',['set_position',['../classEncoder_1_1Encoder.html#a4abc70e4a9b9445008185ec862fac6d5',1,'Encoder::Encoder']]],
  ['setmode_66',['setMode',['../classIMUDriver_1_1IMUDriver.html#a95f4bf7e78d35e732155496118ea0dd6',1,'IMUDriver::IMUDriver']]],
  ['speed_67',['speed',['../classEncoder_1_1Encoder.html#ab55100eccbaac8180d28db89fc633d98',1,'Encoder.Encoder.speed()'],['../classTurntableTask_1_1TurntableTask.html#a6eaf9e485e0f00e01bd491286337f66e',1,'TurntableTask.TurntableTask.speed()']]],
  ['spin_68',['spin',['../classTurntableTask_1_1TurntableTask.html#a4ac7eac21bbdadde0ba01af1abeb1aad',1,'TurntableTask::TurntableTask']]],
  ['state_69',['state',['../classElevatorTask_1_1ElevatorTask.html#ac675e703440ebfba0fe0cce4823bea1c',1,'ElevatorTask.ElevatorTask.state()'],['../classTurntableTask_1_1TurntableTask.html#a8ff69a895a2c8e455a9e55cc16448048',1,'TurntableTask.TurntableTask.state()']]],
  ['stopped_70',['stopped',['../classTurntableTask_1_1TurntableTask.html#acf222bcbe6e18992812ec9ba4782c72f',1,'TurntableTask::TurntableTask']]]
];
